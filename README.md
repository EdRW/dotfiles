# Dotfiles

Backup of my Pop!_OS user configurations.

Dotfile symlinks are managed with [GNU Stow](https://www.gnu.org/software/stow/).

## Installation

Clone this repository into the $HOME directory and run the install script from within the .dotfiles directory.

```bash
git clone https://gitlab.com/EdRW/dotfiles "$HOME/.dotfiles"
chmod +x "$HOME/.dotfiles/install.sh"
$HOME/.dotfiles/install.sh
```

## Post-install



### Install Vscode

Unfortunately the install script will not install vscode so you'll have to do it manually.

See install instructions on the [Vscode Website](https://code.visualstudio.com/docs/setup/linux).


### Install Vscode Extensions

First of all make sure to periodically back up the list of vscode extensions:

```bash
code --list-extensions >> $HOME/.dotfiles/vscode/extensions.txt
```

Once vscode is installed you can reinstall extensions on the new system with this command:

```bash
cat $HOME/.dotfiles/vscode/extensions.txt | xargs -n 1 code --install-extension
```

### Install Docker Engine

Follow the instructions found [Here](https://docs.docker.com/engine/install/ubuntu/)

### Install Google Cloud CLI

Follow the instructions found [Here](https://cloud.google.com/sdk/docs/install)

### Fix GDM Login Screen Configuration

```bash
sudo cp ~/.config/monitors.xml ~gdm/.config/
```

***note:*** consider creating a service to auto update the config when it's changed by the user as shown [Here](https://wiki.archlinux.org/title/GDM#Setup_default_monitor_settings).


### Fix System Clock When Dual-booting Windows

```bash
timedatectl set-local-rtc 1
```

```bash
timedatectl set-local-rtc 1 --adjust-system-clock
```


