function afk --wraps='perl /usr/local/games/asciiquarium/asciiquarium' --wraps='perl ~/.local/bin/asciiquarium' --description 'alias afk=perl ~/.local/bin/asciiquarium'
    perl ~/.local/bin/asciiquarium $argv
end
