function lla --wraps=eza --wraps='eza -lha --hyperlink --git --icons' --description 'alias eza -lha --hyperlink --git --icons'
    eza -lha --hyperlink --git --icons $argv
end
