function unstow --wraps='stow -D' --description 'alias unstow=stow -D'
  stow -D $argv; 
end
