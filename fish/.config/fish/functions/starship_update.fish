function starship_update --wraps='runs the install/update script found at https://starship.rs/guide/#%F0%9F%9A%80-installation' --wraps='the install/update script from starship.rs' --description 'alias starship_update=the install/update script from starship.rs'
    if test "$argv"
        bash -c 'sh -c "$(curl -fsSL https://starship.rs/install.sh)" -- --help'
    else
        bash -c 'sh -c "$(curl -fsSL https://starship.rs/install.sh)"'
    end
end
