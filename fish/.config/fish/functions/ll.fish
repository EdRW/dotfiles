function ll --wraps=ls --wraps='exa -lh' --wraps='exa -lh --icons' --wraps='eza -lh --hyperlink --git --icons --no-permissions --no-user' --wraps=eza --description 'alias eza -lh --hyperlink --git --icons --no-permissions --no-user'
    eza -lh --hyperlink --git --icons --no-permissions --no-user $argv
end
