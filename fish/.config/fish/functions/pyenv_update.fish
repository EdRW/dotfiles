function pyenv_update
    pushd (pyenv root)
    git pull
    popd
end
