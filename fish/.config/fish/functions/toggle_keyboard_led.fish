#!/usr/bin/fish

function toggle_keyboard_led
    set -l led_state (xset -q | head -2 | tail -1 | awk '{print $10}')
    if test "$led_state" = "ffffe7fe"
        # LED is on
        xset led off
    else
        # LED is off
        xset led on
    end
end

if not status is-interactive
    toggle_keyboard_led
end