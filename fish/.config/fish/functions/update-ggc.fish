function update-ggc --wraps='curl -s https://raw.githubusercontent.com/envpcamille/Glamorous-Git-Commits/main/install.sh | bash' --description 'alias update-ggc=curl -s https://raw.githubusercontent.com/envpcamille/Glamorous-Git-Commits/main/install.sh | bash'
  curl -s https://raw.githubusercontent.com/envpcamille/Glamorous-Git-Commits/main/install.sh | bash $argv; 
end
