function kitty-update --wraps='curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin' --description 'alias kitty-update=curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin'
  curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin $argv; 
end
