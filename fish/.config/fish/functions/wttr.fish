function wttr --wraps='curl "wttr.in/Kyoto?n1"' --description 'alias wttr=curl "wttr.in/Kyoto?n1"'
  curl "wttr.in/Kyoto?n1" $argv; 
end
