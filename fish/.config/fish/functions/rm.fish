function rm --wraps='gio trash' --description 'alias rm=gio trash'
  gio trash $argv; 
end
