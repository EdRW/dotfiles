function pdm
    set cmd $argv[1]

    if test "$cmd" = "shell"
        eval (pdm venv activate)
    else
        command pdm $argv
    end
end
