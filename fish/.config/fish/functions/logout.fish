function logout --wraps='gnome-session-quit --no-prompt' --description 'alias logout=gnome-session-quit --no-prompt'
  gnome-session-quit --no-prompt $argv; 
end
