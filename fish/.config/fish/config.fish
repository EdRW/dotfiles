if status is-interactive
    # Commands to run in interactive sessions can go here
    # echo $COLUMNS
    # tput cols
    # stty size
    if test "$COLUMNS" -gt 168
        set color_script rupees
    else
        set color_script (random choice pacman space-invaders)
    end
    set -g fish_greeting (colorscript -e $color_script | string collect) #(color_script_goldfish | string collect)

    set -gx EDITOR vim
    set -gx VISUAL vim
    # set -gx VISUAL /usr/bin/nano


    # You must call it on initialization or listening to directory switching won't work for poetry
    # this function slows down directory jumping too much. Consider re-writing the calls to bash.
    load_nvm

    # init starship prompt
    # check out transient prompts in starship
    # https://starship.rs/advanced-config/#transientprompt-and-transientrightprompt-in-fish
    function starship_transient_prompt_func
        # set cmd_duration (starship module cmd_duration)
        # if test -n "$cmd_duration"
        #     set fill (starship module fill)
        #     set cmd_duration "$fill$cmd_duration\n"
        # end
        set cmd_duration "\n"

        set directory (starship module directory)
        set character (starship module character)
        set prompt "\n$cmd_duration$directory\n$character"
        echo -e $prompt
    end

    function starship_transient_rprompt_func
        starship toggle time
        set time_prompt (starship module time)
        starship toggle time
        if test -n "$time_prompt"
            echo "ran $time_prompt"
        end
    end
    # function my_signal_handler --on-event fish_postexec
    #     echo (starship module cmd_duration)
    #     echo Got fish_postexec event!
    # end

    starship init fish | source
    enable_transience

    # init direnv
    direnv hook fish | source
    set -g direnv_fish_mode eval_on_arrow

end


# # init for pyenv: https://github.com/pyenv/pyenv
status is-login; and command pyenv init --path | source
status is-interactive; and command pyenv init - | source
