#!/bin/bash

{ # This ensures the entire script is downloaded #

# exit when any command fails
set -e

declare -r DOTFILES_DIR="${DOTFILES_DIR:-"$HOME/.dotfiles"}"
declare -r UBUNTU_VERSION="$(lsb_release -rs)"

GREEN='\033[1;32m'
BLUE='\033[1;34m'
RED='\033[1;30m'
NC='\033[0m'

# Navigate to the dotfiles directory (generally ~/.dotfiles)
cd $DOTFILES_DIR

# Navigate to the dotfiles directory (generally ~/.dotfiles)
cd $DOTFILES_DIR

# Update Apt
sudo apt update && sudo apt upgrade

function msg() {
    echo ""
    echo -e "${BLUE}$1${NC}"
    echo ""
}

function ask_user_if_should_run() {
    local yn_promt="$1"
    local function_to_run="$2"

    echo $yn_promt
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) $function_to_run; break;;
            No ) break;;
        esac
    done
}

# deprecated, used eza ppa instead
function configure_exa_ppa() {
    # Required Ubuntu versions for exa (>= 20.10)
    local ver_supports_exa="$(echo "$UBUNTU_VERSION >= 20.10" | bc)"

    # If the Ubuntu version is NOT >= 20.10
    if [ "$ver_supports_exa" -ne "1" ]; then
        # Install Software Properties Common so that we can use add-apt-repository
        msg "Adding ppa for exa..."
        sudo apt install software-properties-common -y
        echo "Add the exa PPA"
        sudo add-apt-repository ppa:spvkgn/exa -y
        sudo apt update
    fi
}

function configure_eza_ppa() {
    # check if eza is already installed
    if ! command -v eza &> /dev/null; then
        # Install Software Properties Common so that we can use add-apt-repository
        msg "Adding ppa for eza..."
        sudo apt update
        sudo apt install -y gpg
        
        sudo mkdir -p /etc/apt/keyrings
        wget -qO- https://raw.githubusercontent.com/eza-community/eza/main/deb.asc | sudo gpg --dearmor -o /etc/apt/keyrings/gierens.gpg
        echo "deb [signed-by=/etc/apt/keyrings/gierens.gpg] http://deb.gierens.de stable main" | sudo tee /etc/apt/sources.list.d/gierens.list
        sudo chmod 644 /etc/apt/keyrings/gierens.gpg /etc/apt/sources.list.d/gierens.list
        sudo apt update
    fi
}

function configure_vscode_repo() {
    if ! command -v code &> /dev/null; then
        # add vs code microsoft repository
        msg "Configuring MS repo for vscode..."
        sudo apt install wget gpg -y
        wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
        sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
        sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
        rm -f packages.microsoft.gpg
        sudo apt install apt-transport-https -y
        sudo apt update
    fi
}

function install_terminal_apps() {
    msg "Installing terminal apps..."
    configure_eza_ppa
    sudo apt install fish neovim neofetch eza git stow unzip wget gpg -y

    # install starship prompt
    msg "Installing starship prompt..."
    curl -sS https://starship.rs/install.sh | sh -s - -y
    
    # install poetry
    msg "Installing poetry..."
    curl -sSL https://install.python-poetry.org | python3 -

    # install nvm
    msg "Installing nvm..."
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash

    if ! command -v pyenv &> /dev/null; then
        # install pyenv
        msg "Installing pyenv..."
        curl https://pyenv.run | bash
        # post-install dependencies
        msg "Installing pyenv recommended python build dependencies..."
        sudo apt install build-essential libssl-dev zlib1g-dev \
        libbz2-dev libreadline-dev libsqlite3-dev curl llvm \
        libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev \
        libffi-dev liblzma-dev libsasl2-dev python3-dev libldap2-dev -y
    fi
}

function install_gui_apps() {
    function install_func() {
        msg "Installing GUI apps"
    	configure_vscode_repo
        sudo apt install code alacritty kitty -y
    }
    ask_user_if_should_run "Install GUI apps?" "install_func"
}


function install_colorscripts() {
    if ! command -v colorscript &> /dev/null; then
        function install_func() {
            # colorscripts!
            msg "Installing colorscript"
            git clone git@gitlab.com:EdRW/shell-color-scripts.git
            pushd shell-color-scripts
            sudo make install
            # fish shell completion
            sudo cp completions/colorscript.fish /usr/share/fish/vendor_completions.d
            popd
            rm -rf shell-color-scripts
        }
        ask_user_if_should_run "Install colorscripts?" "install_func"
    fi
}

function install_asciiquarium_deps() {
    function install_func() {
        # asciiquarium
        # install user scripts
        msg "Installing asciiquarium dependencies..."
        sudo apt install libcurses-perl
        sudo cpan Term::Animation
    }
    ask_user_if_should_run "Install asciiquarium dependencies?" "install_func"
}

function restore_dotfiles() {
    # create dotfile symlinks
    msg "Restoring dotfiles with gnu stow..."
    ls -d $DOTFILES_DIR/*/ | xargs -n 1 basename | xargs stow --adopt 

    changed_files=$(git diff --name-only)
    if [[ ! -z $changed_files ]]; then
        echo -e "${RED}The following files already existed on the system and were not replaced:${NC}"
        echo ""
        printf %"s\n" $changed_files  # Ensure newlines are printed
        printf "\n${BLUE}%s\n\t%s\n\n${NC}" "To overwrite the files on the system run:" "git reset --hard"
    else
        echo -e "${GREEN}Dotfile restore completed!${NC}"
    fi
}

function print_completion_msg(){
    echo -e "${GREEN}Installation Complete!${NC}"
    echo ""
    echo "You may have to install or setup vscode manually though."
    echo "https://code.visualstudio.com/docs/setup/linux"
    echo ""
    echo "To install your extensions run this command"
    echo "cat vscode/extensions.txt | xargs -n 1 code --install-extension"
}

function main() {
    install_terminal_apps
    install_gui_apps
    install_colorscripts
    install_asciiquarium_deps
    restore_dotfiles
    print_completion_msg
}

main

} # This ensures the entire script is downloaded # 
